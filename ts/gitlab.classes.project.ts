import { GitlabGroup } from './gitlab.classes.group';
import * as plugins from './gitlab.plugins';

export interface IGitlabData {
  id: number;
  description: string;
  name: string;
  name_with_namespace: string;
  path: string;
  path_with_namespace: string;
  created_at: string;
  default_branch: string;
  tag_list: string[];
  ssh_url_to_repo: string;
  http_url_to_repo: string;
  web_url: string;
  readme_url: string;
  avatar_url: null;
  forks_count: number;
  star_count: number;
  last_activity_at: string;
  namespace: {
    id: number;
    name: string;
    path: string;
    kind: string;
    full_path: string;
    parent_id: null;
    avatar_url: string;
    web_url: string;
  };
  container_registry_image_prefix: string;
  _links: {
    self: string;
    issues: string;
    merge_requests: string;
    repo_branches: string;
    labels: string;
    events: string;
    members: string;
  };
  packages_enabled: null;
  empty_repo: boolean;
  archived: boolean;
  visibility: 'public' | 'private';
  resolve_outdated_diff_discussions: null;
  container_registry_enabled: boolean;
  issues_enabled: boolean;
  merge_requests_enabled: boolean;
  wiki_enabled: boolean;
  jobs_enabled: boolean;
  snippets_enabled: boolean;
  service_desk_enabled: null;
  service_desk_address: null;
  can_create_merge_request_in: boolean;
  issues_access_level: 'enabled' | 'disabled';
  repository_access_level: 'enabled' | 'disabled';
  merge_requests_access_level: 'enabled' | 'disabled';
  forking_access_level: 'enabled' | 'disabled';
  wiki_access_level: 'enabled' | 'disabled';
  builds_access_level: 'enabled' | 'disabled';
  snippets_access_level: 'enabled' | 'disabled';
  pages_access_level: 'enabled' | 'disabled';
  operations_access_level: 'enabled' | 'disabled';
  analytics_access_level: 'enabled' | 'disabled';
  emails_disabled: null;
  shared_runners_enabled: boolean;
  lfs_enabled: boolean;
  creator_id: number;
  import_status: 'none';
  open_issues_count: number;
  ci_default_git_depth: null;
  ci_forward_deployment_enabled: null;
  public_jobs: boolean;
  build_timeout: number;
  auto_cancel_pending_pipelines: 'enabled' | 'disabled';
  build_coverage_regex: '';
  ci_config_path: '';
  shared_with_groups: [];
  only_allow_merge_if_pipeline_succeeds: boolean;
  allow_merge_on_skipped_pipeline: null;
  restrict_user_defined_variables: boolean;
  request_access_enabled: boolean;
  only_allow_merge_if_all_discussions_are_resolved: boolean;
  remove_source_branch_after_merge: null;
  printing_merge_request_link_enabled: boolean;
  merge_method: 'merge';
  suggestion_commit_message: null;
  auto_devops_enabled: boolean;
  auto_devops_deploy_strategy: 'continuous';
  autoclose_referenced_issues: boolean;
  approvals_before_merge: number;
  mirror: boolean;
  external_authorization_classification_label: '';
  marked_for_deletion_at: null;
  marked_for_deletion_on: null;
  requirements_enabled: boolean;
  security_and_compliance_enabled: null;
  compliance_frameworks: [];
  issues_template: '';
  merge_requests_template: '';
}

export class GitlabProject {
  // STATIC
  public static async getProjectsForGroup(gitlabGroupArg: GitlabGroup): Promise<GitlabProject[]> {
    const response = await gitlabGroupArg.gitlabAccountRef.request(
      'GET',
      `/groups/${gitlabGroupArg.data.id}/projects`,
      {
        per_page: '100',
      }
    );
    const allProjects: GitlabProject[] = [];
    for (const projectData of response) {
      allProjects.push(new GitlabProject(projectData, gitlabGroupArg));
    }
    return allProjects;
  }

  // INSTANCE
  gitlabGroupRef: GitlabGroup;
  data: IGitlabData;

  constructor(dataArg: IGitlabData, gitlabGroupRefArg: GitlabGroup) {
    this.data = dataArg;
    this.gitlabGroupRef = gitlabGroupRefArg;
  }

  public async getFileFromProject(
    filePathArg: string,
    refArg: string
  ): Promise<plugins.smartfile.Smartfile> {
    const response = await this.gitlabGroupRef.gitlabAccountRef.request(
      'GET',
      `/projects/${this.data.id}/repository/files/${filePathArg}`,
      {
        ref: refArg,
      }
    );
    return plugins.smartfile.Smartfile.fromBuffer(
      filePathArg,
      Buffer.from(response.content, response.encoding)
    );
  }

  public async getReadmeAsMarkdown(refArg: string = 'master'): Promise<string> {
    const readmeFile = await this.getFileFromProject('readme.md', refArg);
    return readmeFile.contents.toString('utf8');
  }

  public async getNpmKeywords(refArg: string = 'master'): Promise<string[]> {
    const packageJsonFile = await this.getFileFromProject('package.json', refArg);
    const packageJsonObject: any = JSON.parse(packageJsonFile.contents.toString('utf8'));
    return packageJsonObject.keywords ? packageJsonObject.keywords : [];
  }

  public async getProjectAsArticle(): Promise<plugins.tsclass.content.IArticle> {
    return {
      url: this.data.web_url,
      author: null,
      content: await this.getReadmeAsMarkdown().catch(err => null),
      tags: await this.getNpmKeywords().catch(err => null),
      title: this.data.name,
      timestamp: new Date(this.data.last_activity_at).getTime(),
      featuredImageUrl: null
    }
  }

  public async syncNpmKeywordsToGitlabTopics() {

  }
}
